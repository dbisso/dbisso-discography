## Discography

This plugin create post types and taxonomies to make managing a discography easier

### Status

Under development, but probably usable.

### Dependencies

Currently the plugin requires:

- WordPress >= 3.6
- Plugin: Posts to Posts
- Plugin: Custom Field Suite
- PHP >= 5.3