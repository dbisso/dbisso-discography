<?php
namespace DBisso\Plugin\SkeletonPlugin;

/**
 * Class DBisso\Plugin\SkeletonPlugin\Admin
 */
class Admin {
	static $hooker;

	public function bootstrap( $hooker = null ) {
		if ( !$hooker || !method_exists( $hooker, 'hook' ) )
			throw new \BadMethodCallException( 'Bad Hooking Class. Check that \DBisso\Util\Hooker is loaded.', 1 );

		self::$hooker = $hooker->hook( __CLASS__, $hooker->hook_prefix );
	}
}