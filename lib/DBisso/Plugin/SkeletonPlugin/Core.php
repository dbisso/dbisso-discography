<?php
namespace DBisso\Plugin\SkeletonPlugin;

/**
 * Class DBisso\Plugin\SkeletonPlugin\Core
 */
class Core {
	static $hooker;
	const HOOK_PREFIX = 'skeleton-plugin';

	public function bootstrap( $hooker = null ) {
		if ( !$hooker || !method_exists( $hooker, 'hook' ) )
			throw new \BadMethodCallException( 'Bad Hooking Class. Check that \DBisso\Util\Hooker is loaded.', 1 );

		self::$hooker = $hooker->hook( __CLASS__, self::HOOK_PREFIX );

		register_activation_hook( __FILE__, array( __CLASS__, 'activation' ) );


		if ( is_admin() )
			Admin::bootstrap( $hooker );
		else
			Frontend::bootstrap( $hooker );
	}

	function activation() {}
}