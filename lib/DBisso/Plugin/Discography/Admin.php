<?php
namespace DBisso\Plugin\Discography;

/**
 * Class DBisso\Plugin\Discography\Admin
 */
class Admin {
	static $hooker;

	public function bootstrap( $hooker = null ) {
		if ( !$hooker || !method_exists( $hooker, 'hook' ) )
			throw new \BadMethodCallException( 'Bad Hooking Class. Check that \DBisso\Util\Hooker is loaded.', 1 );

		self::$hooker = $hooker->hook( __CLASS__, $hooker->hook_prefix );
	}

	public function filter_p2p_new_post_args( $args, $ctype, $post_id ) {
		if ( Core::P2P_TRACK_RELEASES == $ctype->name ) {
			$args['post_status'] = 'publish';
		}

		return $args;
	}
}