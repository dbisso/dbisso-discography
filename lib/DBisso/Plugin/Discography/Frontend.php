<?php
namespace DBisso\Plugin\Discography;

/**
 * Class DBisso\Plugin\Discography\Frontend
 */
class Frontend {
	static $hooker;

	public function bootstrap( $hooker = null ) {
		if ( !$hooker || !method_exists( $hooker, 'hook' ) )
			throw new \BadMethodCallException( 'Bad Hooking Class. Check that \DBisso\Util\Hooker is loaded.', 1 );

		self::$hooker = $hooker->hook( __CLASS__, $hooker->hook_prefix );
	}

	/**
	 * Filters and sorts CFS fields based on array
	 * @param  array  $fields Desired fields in order
	 * @return array          Filtered fields
	 */
	public function get_meta_fields( array $fields ) {
		global $cfs;
		$meta = $cfs->get();
		return array_intersect_key( $meta, array_flip( $fields ) );
	}
}

function get_tracks_query() {
	$connected = new \WP_Query(
		array(
			'connected_type' => Core::P2P_TRACK_RELEASES,
			'connected_items' => get_queried_object(),
			'nopaging' => true,
		)
	);

	return $connected;
}

function has_oembed() {
	$oembed_url = get_oembed_url();
	return !empty( $oembed_url );
}

function get_oembed_url() {
	global $cfs;

	$sorted = Frontend::get_meta_fields(
		array(
			'dbdiscog_soundcloud_url',
			'dbdiscog_spotify_url',
		)
	);

	$sorted = array_pop( array_filter( $sorted, 'strlen' ) );

	return $sorted;
}

function get_oembed() {
	return wp_oembed_get( get_oembed_url() );
}

function get_release_data( $short_field = false ) {
	global $cfs;

	$fields = Frontend::get_meta_fields(
		array(
			'dbdiscog_year',
			'dbdiscog_label',
			'dbdiscog_catalogue_no',
		)
	);
	$fields = array_filter( $fields, 'strlen' );

	if ( $short_field && isset( $fields['dbdiscog_' . $short_field] ) ) {
		return $fields['dbdiscog_' . $short_field];
	}

	return $fields;
}

function has_player() {
	return has_oembed();
}

function get_the_player() {
	return get_oembed();
}

function the_player() {
	echo get_the_player();
}


function get_the_artist() {
	return get_the_term_list( get_post()->ID, Taxonomies::ARTIST );
}

function the_artist() {
	echo get_the_artist();
}