<?php
namespace DBisso\Plugin\Discography;

/**
 * Class DBisso\Plugin\Discography\Core
 */
class Core {
	static $hooker;
	const HOOK_PREFIX = 'dbisso-discography';
	const P2P_TRACK_RELEASES = 'tracks_to_releases';

	public function bootstrap( $hooker = null ) {
		if ( !$hooker || !method_exists( $hooker, 'hook' ) )
			throw new \BadMethodCallException( 'Bad Hooking Class. Check that \DBisso\Util\Hooker is loaded.', 1 );

		self::$hooker = $hooker->hook( __CLASS__, self::HOOK_PREFIX );

		register_activation_hook( __FILE__, array( __CLASS__, 'activation' ) );

		PostTypes::bootstrap( $hooker );
		Taxonomies::bootstrap( $hooker );

		if ( is_admin() )
			Admin::bootstrap( $hooker );
		else
			Frontend::bootstrap( $hooker );
	}

	function activation() {}

	public function action_widgets_init() {
		register_widget( __NAMESPACE__ . '\ReleaseWidget' );
	}

	public function action_p2p_init() {
		p2p_register_connection_type(
			array(
				'name' => self::P2P_TRACK_RELEASES,
				'from' => PostTypes::TRACK,
				'to'   => PostTypes::RELEASE,
				'admin_column' => true,
				'admin_dropdown' => true,
				'sortable' => 'to',
			)
		);
	}

	public function action_pre_get_posts( $query ) {
		if ( !is_admin()
			&& $query->is_main_query()
			&& (
				$query->is_post_type_archive( PostTypes::RELEASE )
				|| $query->is_tax( array( Taxonomies::TYPE, Taxonomies::ARTIST, Taxonomies::FORMAT ) )
			)
		) {
			$query->set( 'meta_key', 'dbdiscog_year' );
			$query->set( 'orderby', 'meta_value' );
			$query->set( 'order', 'DESC' );
		}
	}
}