<?php
namespace DBisso\Plugin\Discography;

/**
 * Class DBisso\Plugin\Discography\PostTypes
 */
class PostTypes {
	static $hooker;
	const RELEASE = 'dbisso_release';
	const TRACK   = 'dbisso_track';

	public function bootstrap( $hooker = null ) {
		if ( !$hooker || !method_exists( $hooker, 'hook' ) )
			throw new \BadMethodCallException( 'Bad Hooking Class. Check that \DBisso\Util\Hooker is loaded.', 1 );

		self::$hooker = $hooker->hook( __CLASS__, $hooker->hook_prefix );
	}

	public function action_init() {
		$release_args = array(
			'label'               => 'Releases',
			'description'         => 'A music release.',
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'capability_type'     => 'post',
			'hierarchical'        => true,
			'rewrite'             => array( 'slug' => 'music' ),
			'query_var'           => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'menu_position'       => 10,
			'supports'            => array( 'title', 'revisions', 'thumbnail', ),
			'taxonomies'          => array(),
			'labels'              => array(
				'name'               => 'Releases',
				'singular_name'      => 'Release',
				'menu_name'          => 'Music',
				'add_new'            => 'Add Release',
				'add_new_item'       => 'Add New Release',
				'edit'               => 'Edit',
				'edit_item'          => 'Edit Release',
				'new_item'           => 'New Release',
				'view'               => 'View Release',
				'view_item'          => 'View Release',
				'search_items'       => 'Search Releases',
				'not_found'          => 'No Releases Found',
				'not_found_in_trash' => 'No Releases Found in Trash',
				'parent'             => 'Parent Release',
			)
		);

		register_post_type( self::RELEASE, $release_args );

		$track_args = array(
			'label'               => 'Tracks',
			'description'         => 'A music track.',
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => 'edit.php?post_type=' . self::RELEASE,
			'capability_type'     => 'post',
			'hierarchical'        => false,
			'rewrite'             => array( 'slug' => 'tracks' ),
			'query_var'           => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'menu_position'       => 10,
			'supports'            => array( 'title', 'revisions', 'thumbnail', ),
			'taxonomies'          => array(),
			'labels'              => array(
				'name'               => 'Tracks',
				'singular_name'      => 'Track',
				'menu_name'          => 'Tracks',
				'add_new'            => 'Add Track',
				'add_new_item'       => 'Add New Track',
				'edit'               => 'Edit',
				'edit_item'          => 'Edit Track',
				'new_item'           => 'New Track',
				'view'               => 'View Track',
				'view_item'          => 'View Track',
				'search_items'       => 'Search Tracks',
				'not_found'          => 'No Tracks Found',
				'not_found_in_trash' => 'No Tracks Found in Trash',
				'parent'             => 'Parent Track',
			)
		);

		register_post_type( self::TRACK, $track_args );
	}
}