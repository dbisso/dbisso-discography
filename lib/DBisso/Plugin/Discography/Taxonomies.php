<?php
namespace DBisso\Plugin\Discography;

/**
 * Class DBisso\Plugin\Discography\Taxonomies
 */
class Taxonomies {
	static $hooker;
	const ARTIST = 'dbisso_artist';
	const TYPE = 'dbisso_type';
	const FORMAT = 'dbisso_format';

	public function bootstrap( $hooker = null ) {
		if ( !$hooker || !method_exists( $hooker, 'hook' ) )
			throw new \BadMethodCallException( 'Bad Hooking Class. Check that \DBisso\Util\Hooker is loaded.', 1 );

		self::$hooker = $hooker->hook( __CLASS__, $hooker->hook_prefix );
	}

	public function action_init() {
		register_taxonomy(
			self::ARTIST,
			array(
				PostTypes::TRACK,
				PostTypes::RELEASE,
				'post',
			),
			array(
				'hierarchical'   => true,
				'label'          => _x( 'Artist', 'taxonomy general name', 'dbisso-discography' ),
				'show_ui'        => true,
				'query_var'      => true,
				'rewrite'        => array( 'slug' => 'artist' ),
				'singular_label' => _x( 'Artist', 'taxonomy singular name', 'dbisso-discography' )
			)
		);

		register_taxonomy(
			self::TYPE,
			array(
				PostTypes::TRACK,
				PostTypes::RELEASE,
			),
			array(
				'hierarchical'   => true,
				'label'          => _x( 'Type', 'taxonomy general name', 'dbisso-discography' ),
				'show_ui'        => true,
				'query_var'      => true,
				'rewrite'        => array( 'slug' => 'release/type' ),
				'singular_label' => _x( 'Type', 'taxonomy singular name', 'dbisso-discography' )
			)
		);

		register_taxonomy(
			self::FORMAT,
			array(
				PostTypes::TRACK,
				PostTypes::RELEASE,
			),
			array(
				'hierarchical'   => true,
				'label'          => _x( 'Format', 'taxonomy general name', 'dbisso-discography' ),
				'show_ui'        => true,
				'query_var'      => true,
				'rewrite'        => false,
				'singular_label' => _x( 'Format', 'taxonomy singular name', 'dbisso-discography' )
			)
		);
	}
}